import * as React from 'react';
import {API_BASE_URL} from 'settings';
import { notify } from 'react-notify-toast';

interface IContextProps {
  regions:[];
  setRegions(regions:any):void;
  countries:[];
  setCountries(countries:any):void;
}

export const RegionsContext = React.createContext({} as IContextProps);

export const RegionsContextProvider =  ({ children }) => {
  const [regions,setRegions] = React.useState([]);
  const [countries,setCountries] = React.useState([]);

  const fetchRegions = async () => {
    const fetchRegionsData = await fetch(
      `${API_BASE_URL}regions`,{
        method: 'GET',
      },
    );
    if (fetchRegionsData.status === 200) {
      const jsonData = await fetchRegionsData.json();
      setRegions(jsonData['_embedded']['regions']);
    } else {
      notify.show(
          'Error data. Please try again or contact your administrator.',
          'error',
        );
    }
  };

  const fetchCountries = async () => {
    const fetchCountriesData = await fetch(
      `${API_BASE_URL}countries`,{
        method: 'GET',
      },
    );
    if (fetchCountriesData.status === 200) {
      const jsonData = await fetchCountriesData.json();
      setCountries(jsonData['_embedded']['countries']);
    } else {
      notify.show(
        'Error data. Please try again or contact your administrator.',
        'error',
      );
    }
  };

  React.useEffect(() => {
    fetchRegions();
    fetchCountries();
  },              []);
  const defaultContext = {
    regions,
    countries,
  };

  return (
    <RegionsContext.Provider value={defaultContext}>
      {children}
    </RegionsContext.Provider>
  );
};
