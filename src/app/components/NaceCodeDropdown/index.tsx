import * as React from 'react';
import { Multiselect } from 'multiselect-react-dropdown';
import {API_BASE_URL} from 'settings';

interface IProps {
  onSelect?:Function;
}

export const NaceCodeSelect :React.FC<IProps>= ({onSelect}) => {
  const [naceCodes,setNaceCodes] = React.useState([]);
  const getNaceCodes = async () => {
    const getNaceCodesData = await fetch(`${API_BASE_URL}naceCodes`,{
      method:'GET',
    });
    if(getNaceCodesData.status === 200){
      const jsonData = await getNaceCodesData.json();
      setNaceCodes(jsonData['_embedded']['naceCodes']);
    }
  };
  React.useEffect(() => {
    getNaceCodes();
  },              []);

  return(
    <Multiselect
      options={naceCodes}
      displayValue="naceCodeCode"
      showCheckbox={true}
      onSelect={onSelect}
    />
  );
};
