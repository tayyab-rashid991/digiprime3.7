import './index.scss';

import * as React from 'react';
import {Container, Col, Row, FormGroup} from 'reactstrap';
import {Header} from 'components/Header';
import { Multiselect } from 'multiselect-react-dropdown';
import history from '../../history';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import {RegionsContext,RegionsContextProvider} from 'context/RegionsContext';
import {Button} from 'components/Form/Button';

const LandingPage = () => {
  return(
    <>
      <Header/>
      <Container className="regions">
        <Row>
        <Col sm={12}>
          <div className="region-select">
            <AvForm>
            <AvField
              type="text"
              className="form-control"
              name="nameCompany"
              id="nameCompany"
              placeHolder={'Search'}
              required
              errorMessage=""
              onChange={e => console.log(e)}
              value={''}
            />
            </AvForm>
          </div>
        </Col>
        </Row>
        <FormGroup row className="justify-content-end">
          <div>
            <Button type={'next'} onClick={() => history.push('regions')}>NEXT</Button>
          </div>
        </FormGroup>
      </Container>
    </>
  );
};

export const Landing : React.FC = () => {
  return(
      <LandingPage/>
  );
};
