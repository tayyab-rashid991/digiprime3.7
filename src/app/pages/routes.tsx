import * as React from 'react';
import { Route, Switch } from 'react-router-dom';

// Pages
import {Regions} from 'pages/Regions';
import {FocusSector} from 'pages/AddFocusSector';
import {Landing} from 'pages/LandingPage';
import {GrafanaDashboaard} from 'pages/GrafanaDashboard';
// Layouts
import UnauthenticatedLayout from '../Layouts/UnAuthenticatedLayout';

export const Pages = () => {
  return(
    <Switch>
      <UnauthenticatedLayout path="/regions" exact={true} component={Regions}/>
      <UnauthenticatedLayout path="/" exact={true} component={Landing}/>
      <UnauthenticatedLayout path="/sector" exact={true} component={FocusSector}/>
      <UnauthenticatedLayout path="/dashboard" exact={true} component={GrafanaDashboaard}/>
    </Switch>
  );
};

export default Pages;
