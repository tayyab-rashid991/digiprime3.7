import './index.scss';

import * as React from 'react';
import {Container, Col, Row, FormGroup} from 'reactstrap';
import {Header} from 'components/Header';
import {RegionMap} from './_components/RegionMap';
import {Button} from 'components/Form/Button';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';
import { Multiselect } from 'multiselect-react-dropdown';
import history from '../../history';
import {RegionsContext,RegionsContextProvider} from 'context/RegionsContext';

const RegionsPage = () => {
  const {regions,countries} = React.useContext(RegionsContext);
  const getOptionsArray = (data,name) =>{
    const arr = [];
    data.map((item) =>{
      arr.push(item[name]);
    });
    return arr;
  };
  return(
    <>
      <Header/>
      <Container className="regions">
        <Row>
        <Col sm={12}>
          <h3 className="text-center">Which Region do you represent?</h3>
          <div className="region-select">
            <Multiselect
              options={getOptionsArray(regions,'regionName')}
              isObject={false}
              singleSelect
            />
            <Multiselect
              options={getOptionsArray(countries,'countryName')}
              isObject={false}
              singleSelect
            />
          </div>
        </Col>
        </Row>
        <FormGroup row className="justify-content-between">
            <Button type={'back'} onClick={() => history.push('/')}>BACK</Button>
            <Button type={'next'} onClick={() => history.push('sector')}>NEXT</Button>
        </FormGroup>
      </Container>
    </>
  );
};

export const Regions : React.FC = () => {
  return(
    <RegionsContextProvider>
      <RegionsPage/>
    </RegionsContextProvider>
  );
};
